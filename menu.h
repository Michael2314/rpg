#ifndef __MENU_H__
#define __MENU_H__

#include "cocos2d.h"

class menu: public cocos2d::Layer
{
public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void menuCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(menu);
};

#endif // __HELLOWORLD_SCENE_H__