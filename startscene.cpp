#include "startscene.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include  "help.h"
USING_NS_CC;
using namespace CocosDenshion;
Scene* startscene::createScene()
{
	return startscene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool startscene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
		"play.png",
		"",
		CC_CALLBACK_1(startscene::menuCloseCallback, this));
	if (closeItem == nullptr ||
		closeItem->getContentSize().width <= 0 ||
		closeItem->getContentSize().height <= 0)
	{
		problemLoading("'play.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem->getContentSize().width / 2;
		float y = origin.y + closeItem->getContentSize().height / 2;
		closeItem->setPosition(Vec2(x, y));
	}

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);
	auto bg = Sprite::create("Icon.png");
	bg->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(bg, 0);
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.01);
	SimpleAudioEngine::sharedEngine()->playBackgroundMusic("bgm.mp3",true);
	//SimpleAudioEngine::getInstance()->playEffect("hit.mp3", true, 1.0f, 1.0f, 1.0f);

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem1 = MenuItemImage::create(
		"Help0.png",
		"",
		CC_CALLBACK_1(startscene::menuhelp, this));
	if (closeItem1 == nullptr ||
		closeItem1->getContentSize().width <= 0 ||
		closeItem1->getContentSize().height <= 0)
	{
		problemLoading("'Help0.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem1->getContentSize().width / 2;
		float y = origin.y + visibleSize.height-closeItem1->getContentSize().height / 2;
		closeItem1->setPosition(Vec2(x, y));
	}

	// create menu, it's an autorelease object
	auto menu1 = Menu::create(closeItem1, NULL);
	menu1->setPosition(Vec2::ZERO);
	this->addChild(menu1, 1);

	return true;
}
void startscene::menuCloseCallback(Ref* pSender)
{
	auto scene = HelloWorld::createscene();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.2, scene));
}
void startscene::menuhelp(Ref* pSender)
{
	auto scene = help::create();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.2, scene));
}
