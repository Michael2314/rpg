/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "string.h"
#include "menu.h"
#include "suc.h"
#include "over.h"
#define MAPSIZE 32

USING_NS_CC;
using namespace CocosDenshion;
CCScene* HelloWorld::createscene()
{  
	CCScene* s = CCScene::create();

	auto layer = HelloWorld::create();

	s->addChild(layer);

	return s;
}

bool HelloWorld::init()
{
	auto texture = Director::getInstance()->getTextureCache()->addImage("hero00.png");
	auto closeItem = MenuItemImage::create(
		"menu.png",
		"menu2.png",
		CC_CALLBACK_1(HelloWorld::menulook, this));

	closeItem->setPosition(Vec2::ZERO);

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setScale(0.5f);
	menu->setPosition(Vec2(240+MAPSIZE*2,155+MAPSIZE*2));
	this->addChild(menu, 1);

	map = CCTMXTiledMap::create("GameMap.tmx");
	map->setPosition(Vec2(-480, -480));
	this->addChild(map,0);

	hero = CCSprite::createWithTexture(texture, Rect(0, 0, 32, 32));
	hero->setPosition(ccp(hero->getContentSize().width*20.5, hero->getContentSize().height*20.5));
	map->addChild(hero,3);


	auto listenerkeyPad = EventListenerKeyboard::create();
	listenerkeyPad->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerkeyPad, this);
		
	return true;
}





void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, cocos2d::Event *event) {

	

	CCPoint heropos = ccp((int)(hero->getPositionX() / MAPSIZE), int(map->getContentSize().height / MAPSIZE - hero->getPositionY() / MAPSIZE));
	CCPoint nextpos = heropos;
	CCTMXLayer* layer2 = map->layerNamed("obstacle");
	int gid = 0;
    
	CCTMXLayer* death = map->layerNamed("death");
	int gitdeath = 0;
	gitdeath = death->tileGIDAt(nextpos);
	if (gitdeath) { Close(); Hp = 100; MaxHp = 200; YellowKey = 0; BlueKey = 0; RedKey = 0; Atk = 0; }

	CCTMXLayer* over = map->layerNamed("over");
	int gitover = 0;
	gitover = over->tileGIDAt(nextpos);
	if (gitover) { suc(); Hp = 100; MaxHp = 200; YellowKey = 0; BlueKey = 0; RedKey = 0; Atk = 0; }

	CCTMXLayer* Key = map->layerNamed("key");
	int gitkey = 0;
	gitkey = Key->tileGIDAt(nextpos);
	Key->removeTileAt(heropos);
	if (gitkey) {
		SimpleAudioEngine::getInstance()->playEffect("well.wav", false, 1.0f, 1.0f, 1.0f);
		CCSprite* tx = CCSprite::create("10.png");
		tx->setPosition(ccp(hero->getPositionX(), hero->getPositionY()));
		map->addChild(tx, 3);
		SpriteFrame * frame = nullptr;

		Vector<SpriteFrame *> frameVec;

		for (int i = 1; i <= 24; i++)
		{
			frame = SpriteFrame::create(StringUtils::format("w%d.png", i), Rect(0, 0, 33, 32));
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec);
		ani->setLoops(1);
		ani->setDelayPerUnit(0.05f);
		ani->setRestoreOriginalFrame(true);

		Animate * ac = Animate::create(ani);
		tx->runAction(ac);
	}
	if (gitkey == 2)YellowKey += 1;
	else if (gitkey == 3)BlueKey += 1;
	else if (gitkey == 4)RedKey +=1;

	CCTMXLayer* Weapon = map->layerNamed("weapon");
	int gitweapon = 0;
	gitweapon = Weapon->tileGIDAt(nextpos);
	Weapon->removeTileAt(heropos);
	/*if (gitweapon) {
		CCSprite* tx = CCSprite::create("10.png");
		tx->setPosition(ccp(hero->getPositionX(), hero->getPositionY()));
		map->addChild(tx, 3);
		SpriteFrame * frame = nullptr;

		Vector<SpriteFrame *> frameVec;

		for (int i = 1; i <= 24; i++)
		{
			frame = SpriteFrame::create(StringUtils::format("w%d.png", i), Rect(0, 0, 33, 32));
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec);
		ani->setLoops(1);
		ani->setDelayPerUnit(0.05f);
		ani->setRestoreOriginalFrame(true);

		Animate * ac = Animate::create(ani);
		tx->runAction(ac);
	}*/
	if (gitweapon == 9+1)Atk += 5;
	if (gitweapon == 10+1)Atk += 10;
	if (gitweapon == 11+1)Atk += 15;
	if (gitweapon == 12+1)Atk += 20;
	if (gitweapon == 13+1)Hp += 50, MaxHp += 50;
	if (gitweapon == 14+1)Hp += 100, MaxHp += 100;
	if (gitweapon == 15+1)Hp += 150, MaxHp += 150;
	if (gitweapon == 16+1)Hp += 200, MaxHp += 200;

	CCTMXLayer* Monster = map->layerNamed("object");
	int gitmonster = 0;
	int maxup;
	gitmonster = Monster->tileGIDAt(nextpos);
	if(gitmonster){
		SimpleAudioEngine::getInstance()->playEffect("boom.mp3", false, 1.0f, 1.0f, 1.0f);
		CCSprite* tx = CCSprite::create("10.png");
		tx->setPosition(ccp(hero->getPositionX() ,hero->getPositionY()));
		map->addChild(tx, 3);
		SpriteFrame * frame = nullptr;

		Vector<SpriteFrame *> frameVec;

		for (int i = 1; i <= 7; i++)
		{
			frame = SpriteFrame::create(StringUtils::format("1%d.png", i), Rect(0, 0, 33, 64));
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec);
		ani->setLoops(1);
		ani->setDelayPerUnit(0.15f);
		ani->setRestoreOriginalFrame(true);

		Animate * ac = Animate::create(ani);
		tx->runAction(ac);
	}

	Monster->removeTileAt(heropos);

	if (gitmonster == 21 + 1) { maxup = 5 / 2; if (Atk - 5 < maxup) Hp -= maxup; else Hp += Atk - 5; }
	if (gitmonster == 22 + 1) { maxup = 10 / 2; if (Atk - 10 < maxup) Hp -= maxup; else Hp += Atk - 10; }
	if (gitmonster == 23 + 1) { maxup = 15 / 2; if (Atk - 15 < maxup) Hp -= maxup; else Hp += Atk - 15; }
	if (gitmonster == 24 + 1) { maxup = 30 / 2; if (Atk - 30 < maxup) Hp -= maxup; else Hp += Atk - 30; }
	if (gitmonster == 17 + 1) { maxup = 20 / 2; if (Atk - 20 < maxup) Hp -= maxup; else Hp += Atk - 20; }
	if (gitmonster == 18 + 1) { maxup = 25 / 2; if (Atk - 25 < maxup) Hp -= maxup; else Hp += Atk - 25; }
	if (gitmonster == 19 + 1) { maxup = 30 / 2; if (Atk - 30 < maxup) Hp -= maxup; else Hp += Atk - 30; }
	if (gitmonster == 20 + 1) { maxup = 45 / 2; if (Atk - 45 < maxup) Hp -= maxup; else Hp += Atk - 45; }
	if (gitmonster == 29 + 1) { maxup = 30 / 2; if (Atk - 30 < maxup) Hp -= maxup; else Hp += Atk - 30; }
	if (gitmonster == 30 + 1) { maxup = 35 / 2; if (Atk - 35 < maxup) Hp -= maxup; else Hp += Atk - 35; }
	if (gitmonster == 31 + 1) { maxup = 40 / 2; if (Atk - 40 < maxup) Hp -= maxup; else Hp += Atk - 40; }
	if (gitmonster == 32 + 1) { maxup = 55 / 2; if (Atk - 55 < maxup) Hp -= maxup; else Hp += Atk - 55; }
	if (gitmonster == 25 + 1) { maxup = 100 / 2; if (Atk - 100 < maxup) Hp -= maxup; else Hp += Atk - 100; }
	if (gitmonster == 26 + 1) { maxup = 200 / 2; if (Atk - 200 < maxup) Hp -= maxup; else Hp += Atk - 200; }
	if (gitmonster == 27 + 1) { maxup = 300 / 2; if (Atk - 300 < maxup) Hp -= maxup; else Hp += Atk - 300; }
	if (gitmonster == 28 + 1) { maxup = 700 / 2; if (Atk - 700 < maxup) Hp -= maxup; else Hp += Atk - 700; }
	if (Hp > MaxHp)Hp = MaxHp;

	CCTMXLayer* Water = map->layerNamed("water");
	int gitwater = 0;
	gitwater = Water->tileGIDAt(nextpos);
	Water->removeTileAt(heropos);
	if (gitwater) {
		SimpleAudioEngine::getInstance()->playEffect("well.wav", false, 1.0f, 1.0f, 1.0f);
		CCSprite* tx = CCSprite::create("10.png");
		tx->setPosition(ccp(hero->getPositionX(), hero->getPositionY()));
		map->addChild(tx, 3);
		SpriteFrame * frame = nullptr;

		Vector<SpriteFrame *> frameVec;

		for (int i = 1; i <= 24; i++)
		{
			frame = SpriteFrame::create(StringUtils::format("w%d.png", i), Rect(0, 0, 33, 32));
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec);
		ani->setLoops(1);
		ani->setDelayPerUnit(0.05f);
		ani->setRestoreOriginalFrame(true);

		Animate * ac = Animate::create(ani);
		tx->runAction(ac);
	}
	if (gitwater == 8)
	{
		Hp += 40;
		if (Hp > MaxHp)
		{
			Hp = MaxHp;
		}

	}
	if (gitwater == 9)
	{
		Hp += 80;
		if (Hp > MaxHp)
		{
			Hp = MaxHp;
		}

	}
	if (Hp <= 0) { Close(); Hp = 100; MaxHp = 200; YellowKey = 0; BlueKey = 0; RedKey = 0; Atk = 0; }


	auto texture = Director::getInstance()->getTextureCache()->addImage("hero00.png");
	SpriteFrame * framew = nullptr;

	Vector<SpriteFrame *> frameVecw;

	for (int i = 0; i <= 3; i++)
	{
		framew = SpriteFrame::createWithTexture(texture, Rect(32 * i, 3 * 32, 32, 32));
		frameVecw.pushBack(framew);
	}

	Animation * aniw = Animation::createWithSpriteFrames(frameVecw);
	aniw->setLoops(1);
	aniw->setDelayPerUnit(0.1f);
	aniw->setRestoreOriginalFrame(false);

	Animate * acw = Animate::create(aniw);
	

	SpriteFrame * framea = nullptr;

	Vector<SpriteFrame *> frameVeca;

	for (int i = 0; i <= 3; i++)
	{
		framea = SpriteFrame::createWithTexture(texture, Rect(32 * i, 32, 32, 32));
		frameVeca.pushBack(framea);
	}

	Animation * ania = Animation::createWithSpriteFrames(frameVeca);
	ania->setLoops(1);
	ania->setDelayPerUnit(0.1f);
	ania->setRestoreOriginalFrame(false);

	Animate * aca = Animate::create(ania);

	SpriteFrame * frames = nullptr;

	Vector<SpriteFrame *> frameVecs;

	for (int i = 0; i <= 3; i++)
	{
		frames = SpriteFrame::createWithTexture(texture, Rect(32 * i, 0, 32, 32));
		frameVecs.pushBack(frames);
	}

	Animation * anis = Animation::createWithSpriteFrames(frameVecs);
	anis->setLoops(1);
	anis->setDelayPerUnit(0.1f);
	anis->setRestoreOriginalFrame(false);

	Animate * acs = Animate::create(anis);

	SpriteFrame * framed = nullptr;

	Vector<SpriteFrame *> frameVecd;

	for (int i = 0; i <= 3; i++)
	{
		framed = SpriteFrame::createWithTexture(texture, Rect(32 * i, 32*2, 32, 32));
		frameVecd.pushBack(framed);
	}

	Animation * anid = Animation::createWithSpriteFrames(frameVecd);
	anid->setLoops(1);
	anid->setDelayPerUnit(0.1f);
	anid->setRestoreOriginalFrame(false);

	Animate * acd = Animate::create(anid);






	switch (keyCode) {
	case cocos2d::EventKeyboard::KeyCode::KEY_W: {
		nextpos.y -= 1;
		gid = layer2->tileGIDAt(nextpos);
		if (gid)return;
		CCTMXLayer* Door = map->layerNamed("door");
		int gitdoor = 0;
		gitdoor = Door->tileGIDAt(nextpos);
		Door->removeTileAt(heropos);
		if(gitdoor)
			SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
		if (gitdoor == 5)
		{
			if (YellowKey > 0)
			{
				YellowKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}
	
	
		if (gitdoor == 6)
		{
			if (BlueKey > 0)
			{
				BlueKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}
	
		if (gitdoor == 7)
		{
			if (RedKey > 0)
			{
				RedKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}

		if (map->getContentSize().height + map->getPositionY() > (320+MAPSIZE/2))
		{
			if (hero->getPositionY()+map->getPositionY() > 160)
			{
				CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(0, -MAPSIZE));
				CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
				map->runAction(moveby);
			}
			//map->setPosition(ccp(map->getPositionX(), map->getPositionY() - MAPSIZE));

		
			
			
			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(0, MAPSIZE));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(acw);
			
			
		}
		else if (hero->getPositionY()<map->getContentSize().height-MAPSIZE)
		{
			CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(0, MAPSIZE));
			CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
			hero->runAction(moveby);
			hero->runAction(acw);
		}







		/*CCMoveBy *moveby = CCMoveBy::create(0.5f, ccp(0, MAPSIZE));
		CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
		hero->runAction(moveby);*/
		break;
	}
	case cocos2d::EventKeyboard::KeyCode::KEY_S: {

		nextpos.y += 1;
		gid = layer2->tileGIDAt(nextpos);
		if (gid)return;
		CCTMXLayer* Door = map->layerNamed("door");
		int gitdoor = 0;
		gitdoor = Door->tileGIDAt(nextpos);
		Door->removeTileAt(heropos);
		if (gitdoor)
			SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
		if (gitdoor == 5)
		{
			if (YellowKey > 0)
			{
				YellowKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}


		if (gitdoor == 6)
		{
			if (BlueKey > 0)
			{
				BlueKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}

		if (gitdoor == 7)
		{
			if (RedKey > 0)
			{
				RedKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}
		if (map->getPositionY() < -MAPSIZE/2)
		{
			if(hero->getPositionY()+map->getPositionY()<160)
				{
					CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(0, +MAPSIZE));
					CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
					map->runAction(moveby);
				}
			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(0, -MAPSIZE));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(acs);
		}
		else if (hero->getPositionY() > MAPSIZE)
		{
			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(0, -MAPSIZE));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(acs);
		}
		
		
		
		
		
		
		
	
		break;
	}
	case cocos2d::EventKeyboard::KeyCode::KEY_A: {

		nextpos.x -= 1;
		gid = layer2->tileGIDAt(nextpos);
		if (gid)return;
		CCTMXLayer* Door = map->layerNamed("door");
		int gitdoor = 0;
		gitdoor = Door->tileGIDAt(nextpos);
		Door->removeTileAt(heropos);
		
		if (gitdoor == 5)
		{
			if (YellowKey > 0)
			{
				YellowKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}


		if (gitdoor == 6)
		{
			if (BlueKey > 0)
			{
				BlueKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}

		if (gitdoor == 7)
		{
			if (RedKey > 0)
			{
				RedKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}
		if (map->getPositionX() < -MAPSIZE/2)
		{
			if (hero->getPositionX() + map->getPositionX() < 240)
			{
				CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(+MAPSIZE, 0));
				CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
				map->runAction(moveby);
			}
			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(-MAPSIZE, 0));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(aca);
		}
		else if (hero->getPositionX() > MAPSIZE)
		{
			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(-MAPSIZE, 0));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(aca);
		}








		
		break;
	}
	case cocos2d::EventKeyboard::KeyCode::KEY_D: {

		nextpos.x += 1;
		gid = layer2->tileGIDAt(nextpos);
		if (gid)return;
		CCTMXLayer* Door = map->layerNamed("door");
		int gitdoor = 0;
		gitdoor = Door->tileGIDAt(nextpos);
		Door->removeTileAt(heropos);

			
		if (gitdoor == 5)
		{
			if (YellowKey > 0)
			{
				YellowKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}


		if (gitdoor == 6)
		{
			if (BlueKey > 0)
			{
				BlueKey -= 1;
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);
			}
			else return;
		}

		if (gitdoor == 7)
		{
			if (RedKey > 0)
			{
				SimpleAudioEngine::getInstance()->playEffect("opendoor.mp3", false, 1.0f, 1.0f, 1.0f);

				RedKey -= 1;
			}
			else return;
		}
		if (map->getContentSize().width + map->getPositionX() > (480 +MAPSIZE/2))
		{
			if (hero->getPositionX() + map->getPositionX() > 240)
			{
				CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(-MAPSIZE, 0));
				CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
				map->runAction(moveby);
			}
			//map->setPosition(ccp(map->getPositionX(), map->getPositionY() - MAPSIZE));




			CCMoveBy *moveby1 = CCMoveBy::create(0.1f, ccp(MAPSIZE, 0));
			CCActionInterval *SineOUt1 = CCEaseSineInOut::create(moveby1);
			hero->runAction(moveby1);
			hero->runAction(acd);

		}
		else if (hero->getPositionX() < map->getContentSize().width - MAPSIZE)
		{
			CCMoveBy *moveby = CCMoveBy::create(0.1f, ccp(MAPSIZE,0 ));
			CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
			hero->runAction(moveby);
			hero->runAction(acd);
		}




		/*CCMoveBy *moveby = CCMoveBy::create(0.5f, ccp(MAPSIZE, 0));
		CCActionInterval *SineOUt = CCEaseSineInOut::create(moveby);
		hero->runAction(moveby);*/
		break;
	}

	



	default:
		break;
	}

	
}


void HelloWorld::Close()
{
	auto scene = over::createScene();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.2, scene));

}

void HelloWorld::suc()
{
	auto scene = suc::createScene();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.2, scene));
}

void HelloWorld::menulook(cocos2d::Ref* pSender)
{
	auto menusence = menu::createScene();

	Director::getInstance()->pushScene(menusence);
	
}




Scene* menu::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = menu::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}
bool menu::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.


	char maxhp[30];
	_itoa_s(MaxHp,maxhp, 10);
	
	char hp[30];
	_itoa_s(Hp, hp, 10);
	char yk[30];
	_itoa_s(YellowKey,yk, 10);
	char bk[30];
	_itoa_s(BlueKey,bk, 10);
	char rk[30];
	_itoa_s(RedKey, rk, 10);
	char atk[30];
	_itoa_s(Atk, atk, 10);

	auto labelmaxhp2 = Label::createWithTTF(maxhp, "fonts/Marker Felt.ttf", 24);
	labelmaxhp2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - MAPSIZE));
	this->addChild(labelmaxhp2, 1);

	auto labelhp2 = Label::createWithTTF(hp, "fonts/Marker Felt.ttf", 24);
	labelhp2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - 2.5*MAPSIZE));
	this->addChild(labelhp2, 1);

	auto labelblue2 = Label::createWithTTF(bk, "fonts/Marker Felt.ttf", 24);
	labelblue2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - 5.5 * MAPSIZE));
	this->addChild(labelblue2, 1);

	auto labelyellow2 = Label::createWithTTF(yk, "fonts/Marker Felt.ttf", 24);
	labelyellow2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - 4 * MAPSIZE));
	this->addChild(labelyellow2, 1);

	auto labelred2 = Label::createWithTTF(rk, "fonts/Marker Felt.ttf", 24);
	labelred2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - 7 * MAPSIZE));
	this->addChild(labelred2, 1);

	auto labelatk2 = Label::createWithTTF(atk, "fonts/Marker Felt.ttf", 24);
	labelatk2->setPosition(Vec2(240 + MAPSIZE * 2, 320 - 8.5 * MAPSIZE));
	this->addChild(labelatk2, 1);




	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
		"return.png",
		"",
		CC_CALLBACK_1(menu::menuCallback, this));

	closeItem->setPosition(Vec2(475*1.5-MAPSIZE,315*1.5-MAPSIZE));

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setScale(0.5f);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label

	auto labelmaxhp = Label::createWithTTF("MaxHp", "fonts/Marker Felt.ttf", 24);
	labelmaxhp->setPosition(Vec2(240-MAPSIZE*2,320-MAPSIZE));
	this->addChild(labelmaxhp, 1);

	auto labelhp = Label::createWithTTF("Hp", "fonts/Marker Felt.ttf", 24);
	labelhp->setPosition(Vec2(240 - MAPSIZE * 2, 320 - 2.5*MAPSIZE));
	this->addChild(labelhp, 1);

	auto labelblue = Label::createWithTTF("Blue key", "fonts/Marker Felt.ttf", 24);
	labelblue->setPosition(Vec2(240 - MAPSIZE * 2, 320 - 5.5 * MAPSIZE));
	this->addChild(labelblue, 1);

	auto labelyellow = Label::createWithTTF("Yellow key", "fonts/Marker Felt.ttf", 24);
	labelyellow->setPosition(Vec2(240 - MAPSIZE * 2, 320 - 4 * MAPSIZE));
	this->addChild(labelyellow, 1);

	auto labelred = Label::createWithTTF("Red key", "fonts/Marker Felt.ttf", 24);
	labelred->setPosition(Vec2(240 - MAPSIZE * 2, 320 - 7 * MAPSIZE));
	this->addChild(labelred, 1);

	auto labelatk = Label::createWithTTF("ATK", "fonts/Marker Felt.ttf", 24);
	labelatk->setPosition(Vec2(240 - MAPSIZE * 2, 320 - 8.5 * MAPSIZE));
	this->addChild(labelatk, 1);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto labelmaxhp1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelmaxhp1->setPosition(Vec2(240, 320 - MAPSIZE));
	this->addChild(labelmaxhp1, 1);

	auto labelhp1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelhp1->setPosition(Vec2(240 , 320 - 2.5*MAPSIZE));
	this->addChild(labelhp1, 1);

	auto labelblue1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelblue1->setPosition(Vec2(240 , 320 - 5.5 * MAPSIZE));
	this->addChild(labelblue1, 1);

	auto labelyellow1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelyellow1->setPosition(Vec2(240 , 320 - 4 * MAPSIZE));
	this->addChild(labelyellow1, 1);

	auto labelred1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelred1->setPosition(Vec2(240 , 320 - 7 * MAPSIZE));
	this->addChild(labelred1, 1);

	auto labelatk1 = Label::createWithTTF(":", "fonts/Marker Felt.ttf", 24);
	labelatk1->setPosition(Vec2(240 , 320 - 8.5 * MAPSIZE));
	this->addChild(labelatk1, 1);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	auto sprite = Sprite::create("bg.jpg");
	// position the sprite on the center of the screen
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	// add the sprite as a child to this layer
	this->addChild(sprite, 0);
	return true;
}

void menu::menuCallback(Ref* pSender)
{
	Director::getInstance()->popScene();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
