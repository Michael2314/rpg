#ifndef __HELP_H__
#define __HELP_H__
#include "cocos2d.h"
USING_NS_CC;
class help : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void menuhelp(cocos2d::Ref* pSender);
	CREATE_FUNC(help);
};

#endif