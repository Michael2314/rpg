#include "help.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include "startscene.h"
Scene* help::createScene()
{
	return help::create();
}
bool help::init()
{
	auto help1 = Sprite::create("help00.png");
	help1->setPosition(Vec2(240,160));
	this->addChild(help1, 0);


	auto closeItem1 = MenuItemImage::create(
		"return00.png",
		"",
		CC_CALLBACK_1(help::menuhelp, this));
	
	float x = 480-16;
	float y = 16;
		closeItem1->setPosition(Vec2(x, y));
	


	auto menu1 = Menu::create(closeItem1, NULL);
	menu1->setPosition(Vec2::ZERO);
	this->addChild(menu1, 1);


	return true;

}

void help::menuhelp(cocos2d::Ref* pSender)
{auto scene = startscene::create();
CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.2, scene)); }