#ifndef __NewScene_H__
#define __NewScene_H__

#include "cocos2d.h"

class over: public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void overCallback(cocos2d::Ref* pSender);
	void menuCloseCallback(cocos2d::Ref* pSender);
	CREATE_FUNC(over);
};

#endif